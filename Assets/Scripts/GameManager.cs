﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.Linq;

public class GameManager : MonoBehaviour
{

    // Base du jeu

    public Question[] questions;
    private static List<Question> questionsNonrep;

    private Question questionActuelle;
    
    // Interraction UI
    [SerializeField]
    private Text QuestionText;

    [SerializeField]
    private float TempsTransition = 1f;

    [SerializeField]
    private float TempsParQuestion = 6f;

    [SerializeField]
    private Text TimerText;

    [SerializeField]
    private Text PointsText;

    [SerializeField]
    private int points;


    private bool Reset = false;

    void Start()
    {
        points = PlayerPrefs.GetInt("Points");
        if (questionsNonrep == null ||questionsNonrep.Count == 0)
        {
            questionsNonrep = questions.ToList<Question>();
        }

        GetQuestionAleatoire();
        SetupPoints();
        Debug.Log("La question ou affirmation: " + questionActuelle.qText + " est " + questionActuelle.isTrue);
    }

    void Update()
    {
        TempsParQuestion = TempsParQuestion - Time.deltaTime;
        TimerText.text = "Temps restant: " + Mathf.Round(TempsParQuestion)+"s"; // TempsParQuestion = 5.7s -> 6s ; TempsParQuestion = 5.2s -> 5s 
        if(TempsParQuestion < 0)
        {
            TempsParQuestion = 0;
            if(Reset == false)
            {
                PlusDeTemps();
                Reset = true;
            }
            //PlusDeTemps();
        }
    }

    void PlusDeTemps()
    {
        Debug.Log("ECHEC !");
        points = points - 1;
        PlayerPrefs.SetInt("Points", points);
        StartCoroutine(TransitionProchaineQuestion());
    }

    void GetQuestionAleatoire ()
    {
        int randomQuestionIndex = Random.Range(0, questionsNonrep.Count);
        questionActuelle = questionsNonrep[randomQuestionIndex];

        QuestionText.text = questionActuelle.qText;

    }

    void SetupPoints ()
    {
        PointsText.text = "Points: " + points.ToString();
        Debug.Log(points);
    }


    IEnumerator TransitionProchaineQuestion ()
    {
        questionsNonrep.Remove(questionActuelle);

        yield return new WaitForSeconds(TempsTransition);

        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void UtilisateurSelectVrai () 
    {
        if (questionActuelle.isTrue)
        {
            Debug.Log("Bravo !");
            points = points + 1;
            PlayerPrefs.SetInt("Points", points);
        } else 
        {
            Debug.Log("ECHEC !");
            points = points - 1;
            PlayerPrefs.SetInt("Points", points);
        }

        StartCoroutine(TransitionProchaineQuestion());
    }

    public void UtilisateurSelectFaux ()
    {
        if (!questionActuelle.isTrue)
        {
            Debug.Log("Bravo !");
            points = points + 1;
            PlayerPrefs.SetInt("Points", points);
        } else 
        {
            Debug.Log("ECHEC !");
            points = points - 1;
            PlayerPrefs.SetInt("Points", points);
        }

        StartCoroutine(TransitionProchaineQuestion());
    }

}
